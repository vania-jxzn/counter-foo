import React, { useState } from "react";

/**
 * If count is 0 display "POOP_TEXT"
 * If count is divisible by 3 display "COUNTER_TEXT"
 * If count is divisible by 5 display "FOO_TEXT"
 * If count is divisible by 3 && 5 display "COUNTER_FOO_TEXT"
 * If count is not divisible by 3, 5 display "POOP_TEXT"
 * The increment button should increase the "count" variable
 * The decrement button should decrease the "count" variable
 * The "count" variable should never go below 0
 */

const COUNTER_TEXT = "Counter";
const FOO_TEXT = "Foo";
const COUNTER_FOO_TEXT = "🙅🏿‍♂️ Counter Foo 🙅🏿‍♂️";
const POOP_TEXT = "💩";

function App() {
  const [counter, setCounter] = useState(0)

  function incrementCount() {
    setCounter(preCount => preCount + 1 )

  }

  function decrementCount() {
    setCounter(preCount => preCount - 1 )
  }


  let text = POOP_TEXT
  // if ({counter}  % 3 == 0 ){
  //   COUNTER_TEXT
  // } 
  // else if ({counter}  % 5 == 0 ) {
  //   FOO_TEXT
  // } 
  // else if ({counter}  % 3 == 0 && {counter}  % 5 == 0) {
  //   COUNTER_FOO_TEXT
  // }  

  // else if ({counter}  % 3 != 0 && {counter}  % 5 != 0) {
  //   POOP_TEXT
  // }



  return (
    <div style={styles.app}>
      <h1>{counter}</h1>
      <h3>{text}</h3>
      <div style={styles.buttons}>
        <button onClick={incrementCount}>Increment</button>
        <button onClick={decrementCount}>Decrement</button>
      </div>
    </div>
  );
}

const styles = {
  app: {
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  buttons: {
    display: "flex",
    flexDirection: "row",
  },
};
export default App;
